# Generated by Django 2.1.5 on 2019-02-12 04:53

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('person_name', models.CharField(max_length=200)),
                ('person_number', models.PositiveIntegerField(default=1000000000, validators=[django.core.validators.MaxValueValidator(9999999999), django.core.validators.MinValueValidator(1000000000)])),
                ('person_birthday', models.DateField(validators=[django.core.validators.MaxValueValidator(datetime.date(2019, 2, 11))])),
                ('person_calendar_data', models.TextField()),
            ],
        ),
    ]
