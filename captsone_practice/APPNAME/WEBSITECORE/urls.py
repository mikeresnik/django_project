"""WEBSITECORE URL Configuration

'main/urls.py' in sentdex tutorial.

"""
from django.urls import path

from . import views

app_name = "WEBSITECORE"

urlpatterns = [
	path("", views.homepage, name="homepage"),
	path("profile", views.profile, name="profile"),
	path("register", views.register, name="register"),
	path("login", views.login, name="login")
]
