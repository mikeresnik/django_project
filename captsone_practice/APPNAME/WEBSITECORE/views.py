from django.shortcuts import render
from django.http import HttpResponse
from .models import Person

# In WEBSITECORE folder
# In main/views.py in sentdex tutorial
# Create your views here.
def homepage(request):
	query_set = Person.objects.all()
	return render(
		# Pass the request
		request=request,
		# Where to find template
		template_name="WEBSITECORE/home.html",
		# Pass in variable 'people' using Person.objects.all
		context={'people' : query_set}
	)

def profile(request):
	query_set = Person.objects.all()
	return render(
		# Pass the request
		request=request,
		# Where to find template
		template_name="WEBSITECORE/profile.html",
		# Pass in variable 'people' using Person.objects.all
		context={'people' : query_set}
	)

def register(request):
	return render(
	request = request,
	template_name = "WEBSITECORE/register.html",
	context={}
	)

def login(request):
	return render(
	request = request,
	template_name = "WEBSITECORE/login.html",
	context={}
	)