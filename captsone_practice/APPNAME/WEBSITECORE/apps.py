from django.apps import AppConfig


class WebsitecoreConfig(AppConfig):
    name = 'WEBSITECORE'
