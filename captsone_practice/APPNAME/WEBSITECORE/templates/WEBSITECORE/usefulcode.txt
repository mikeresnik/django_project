<!DOCTYPE html>
<html>
<body>


<form action="/action_page.php">
  First Name: <input type="text" name="firstname" pattern="^[a-zA-Z]+$" title="Please enter a valid first name.">
  <input type="submit">
</form>

<form action="/action_page.php">
  Last Name: <input type="text" name="lastname" pattern="^[a-zA-Z]+$" title="Please enter a valid last name.">
  <input type="submit">
</form>

<form action="/action_page.php">
  Phone Number: <input type="tel" name="phone" pattern="^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$" title="Please enter a valid phone number. (XXX) XXX-XXXX">
  <input type="submit">
</form>


<form action="/action_page.php">
  Email: <input type="email" name="email" pattern="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$" title="Please enter a valid phone number. (XXX) XXX-XXXX">
  <input type="submit">
</form>

<form action="/action_page.php">
  Confirm Email: <input type="email" name="email" pattern="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$" title="Please enter a valid phone number. (XXX) XXX-XXXX">
  <input type="submit">
</form>

<form action="/action_page.php">
  Birthday: <input type="date" name="birthday" pattern="(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)$" title="Please enter a valid phone number. (XXX) XXX-XXXX">
  <input type="submit">
</form>

<form action="/action_page.php">
  Organization: <input type="text" name="organization" pattern="^[a-zA-Z0-9 ]*$" title="Please enter a valid organization.">
  <input type="submit">
</form>



</body>
</html>
